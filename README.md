# Documentation

### System Context diagram
![](images/1st_diagram.png)

### Container diagram
![](images/2nd_diagram.png)

## Microservices

This is the root of a system which implements basic library functions. System consist of several microservices:

User service - https://gitlab.fel.cvut.cz/sonntdav/swa-user-service

Course service - https://gitlab.fel.cvut.cz/barbutim/swa-course-service

Room register - https://gitlab.fel.cvut.cz/jarymiro/swa-room-service

Inventory service - https://gitlab.fel.cvut.cz/svobola7/inventory-service

## Description

1. User is divided into Student and/or Professor.
2. Student can enroll for the course.
3. Professor can teach a course.
4. Courses are divided into Lectures, Exercises a Laboratories
5. Each parallel of a Course has its own Room.
6. Professor can borrow stuff from the Inventory.
7. Stuff in Rooms has to be part of the Inventory.
8. Stuff in Inventory must have a valid revision.
9. Rooms have a certain capacity.

## How to run microservices

All the configurations are described in docker-compose.yml files. To run the project, you must have installed Docker Desktop application on your computer and run the following command, when inside of the microservice folder: 

`docker-compose up`

## Microservice ports

user-service - 8081
course-service - 8082
room-service - 8083
inventory-service - 8080

## Scoreboard


1. Codebase – one codebase tracked in revision control, many deploys
- [X] separate repositories for each microservice (1)
- [X] continuous integration to build, test and create the artefact (3)
- [X] implement some tests and test each service separately (unit tests, integration tests) (5)

2. Dependencies – explicitly declare and isolate dependencies
- [X] preferably Maven project with pom.xml
- [X] eventually gradle project or other

3. Config 
- [X] configuration of services provided via environmental properties (1)
- [ ] eventually as configuration as code (bonus: 0.5)

4. Backing services – treat backing services as attached resources
- [X] backing services like database and similar will be deployed as containers too (1)

5. Build, release, run – strictly separate build and run stages
- [X] CI & docker 
- [ ] eventually upload your images to docker hub (bonus: 1)

6. - [X] Processes – execute the app as one or more stateless processes (1)

7. - [X] Port binding – export services via port binding (1)

8. Disposability – maximize robustness with fast startup and graceful shutdown
- [X] ability to stop/restart service without catastrophic failure for the rest (2)

9. Dev/prod parity – keep development, staging, and production as similar as possible
- [X] 3-5 integration tests (5)
- [X] services will be deployed as containers (2)

10. Logs – treat logs as event streams
- [X] log into standard output (1)
- [ ] eventually collect logs in Elastic or in other tool (bonus: 0.5)

11. Communication 
- [X] REST API defined using Open API standard (Swagger) (2)
- [X] auto-generated in each service (1)
- [X] clear URLs (2)
- [X] clean usage of HTTP statuses (2)
- [ ] eventually message based asynchronous communication via queue (bonus: 1)

12. Transparency – the client should never know the exact location of a service.
- [X] service discovery (2)
- [X] eventually client side load balancing (bonus: 0.5)
- [ ] or workload balancing (bonus: 0.5)

13. Health monitoring – a microservice should communicate its health
- [X] Actuators (1)
- [ ] eventually Elastic APM or other tool (bonus: 1)

14. - [X] Design patterns – use the appropriate patterns

15. - [X] Scope – use domain driven design or similar to design the microservices (5)

16. - [X] Documentation – visually communicate architecture of your system (5)
