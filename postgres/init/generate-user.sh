#!/bin/bash
set -e

DBNAME="user_db"

# Create database if it does not exist
echo "SELECT 'CREATE DATABASE $DBNAME' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '$DBNAME')\gexec" | psql

# Execute SQL commands to create users and grant privileges
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$DBNAME" <<-EOSQL
    CREATE USER my_user PASSWORD 'secret';
    GRANT ALL PRIVILEGES ON DATABASE $DBNAME TO my_user;
EOSQL
